%{!?dist: %define dist .el7.cern}
Name: cern-get-keytab
Version: 1.5.6
Release: 1%{?dist}
Summary: Utility to acquire Kerberos keytab(s) at CERN
Group: Applications/System
Source: %{name}-%{version}.tgz
License: GPLv3
Vendor: CERN
URL: http://cern.ch/linux/

BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch

%if 0%{?rhel} >= 7
BuildRequires: perl-podlators
%else
BuildRequires: perl
%endif

Requires: msktutil >= 1.1
Requires: tdb-tools
Requires: coreutils
%if 0%{?rhel} == 7
Requires: perl-WWW-Curl
Requires: perl-Authen-Krb5
Requires: perl(XML::Simple)
Requires: perl(Term::ReadKey)
%else
Requires: krb5-workstation
Requires: python3-pexpect
Requires: python3-PyYAML
Requires: python3-requests
%endif

Provides: cern-merge-keytab

%if 0%{?rhel} >= 6
Requires: CERN-CA-certs >= 20120322-8.slc6
Requires: ca-certificates >= 2010.63-3.el6_1.5
%else
Requires: CERN-CA-certs >= 20120322-8.slc5
Requires: openssl
%endif

%description
%{name} is a CERN utility which stores in local keytab file(s)
host / services identities acquired from CERN Active Directory
KDC. Please note that this tool is CERN specific and of no
use outside CERN network.

%prep
%setup -q

%build
pod2man %{name}.pl > %{name}.3
pod2man cern-merge-keytab.pl > cern-merge-keytab.3

%install
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}
mkdir -p $RPM_BUILD_ROOT/%{_sbindir}
mkdir -p $RPM_BUILD_ROOT/%{_libexecdir}
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man3

%if 0%{?rhel} == 7
install -m 755 %{name}.pl $RPM_BUILD_ROOT/%{_sbindir}/%{name}
install -m 755 cern-merge-keytab.pl $RPM_BUILD_ROOT/%{_sbindir}/cern-merge-keytab
%else
# Python version used currently only on 8 and 9
install -m 755 %{name}.py $RPM_BUILD_ROOT/%{_sbindir}/%{name}
install -m 755 cern-merge-keytab.py $RPM_BUILD_ROOT/%{_sbindir}/cern-merge-keytab
%endif
install -m 755 cern-get-keytab-samba-workaround $RPM_BUILD_ROOT/%{_libexecdir}/cern-get-keytab-samba-workaround
install -m 644 %{name}.3 $RPM_BUILD_ROOT/%{_mandir}/man3/%{name}.3
install -m 644 cern-merge-keytab.3 $RPM_BUILD_ROOT/%{_mandir}/man3/cern-merge-keytab.3
install -m 755 cern-get-keytab.yaml $RPM_BUILD_ROOT/%{_sysconfdir}/cern-get-keytab.yaml

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_sbindir}/cern-get-keytab
%{_sbindir}/cern-merge-keytab
/usr/libexec/cern-get-keytab-samba-workaround
#{_sbindir}/cern-get-dualboot-keytab
%{_mandir}/man3/cern-get-keytab.3*
%{_mandir}/man3/cern-merge-keytab.3*
#{_mandir}/man3/cern-get-dualboot-keytab.3*
%attr(0644, root, root)%config(noreplace)%{_sysconfdir}/cern-get-keytab.yaml
%doc README

%changelog
* Wed Jul 05 2023 Ben Morrice <ben.morrice@cern.ch> - 1.5.6-1
- don't error out if the user doesn't provide a user password

* Thu Jun 29 2023 Ben Morrice <ben.morrice@cern.ch> - 1.5.5-1
- ensure passwords are not expanded by shell=True in subprocess calls

* Mon Jun 26 2023 Ben Morrice <ben.morrice@cern.ch> - 1.5.4-1
- fix selinux file system detection

* Mon Jun 05 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.5.3-1
- fix bug in reset_password

* Fri Apr 21 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.5.2-1
- show an error when no ports are available

* Tue Apr 04 2023 Ben Morrice <ben.morrice@cern.ch> - 1.5.1-1
- fix selinux routine (detection of keytab location on /afs for 8/9)

* Mon Mar 13 2023 Ben Morrice <ben.morrice@cern.ch> - 1.4.1-1
- Deprecate DES support (OTG0076290)

* Fri Jan 20 2023 Ben Morrice <ben.morrice@cern.ch> - 1.3.1-1
- add python port of cern-merge-keytab (only for el8 and el9)
- add krb5-workstation and coreutils as dependencies

* Wed Jan 18 2023 Ben Morrice <ben.morrice@cern.ch> - 1.2.1-1
- use the KDC alias cerndcqa when using lxkerbwinqa

* Mon Dec 12 2022 Ben Morrice <ben.morrice@cern.ch> - 1.2.0-2
- add back perl-Authen-Krb5 on el8/el9 (for cern-merge-keytab)

* Wed Aug 17 2022 Ben Morrice <ben.morrice@cern.ch> - 1.2.0-1
- Port to python (only for el8 and el9)

* Wed Jun 8 2022 Ulrich Schwickerath - 1.1.6-2
- improve man page

* Thu May 19 2022 Ulrich Schwickerath - 1.1.6-1
- add support for cname keytab
- add support for a configuration file for lxkerbwin config

* Fri May 21 2021 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1.5-15
- bugfix: do not attempt to set selinux context when not possible
- bugfix: fix some warnings

* Thu May 20 2021 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-14
- bugfix: do not attempt to parse a corrupt keytab

* Tue Nov 10 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1.5-13
- require msktutil > 1.1 to remove the previous hack

* Mon Nov 09 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1.5-12
- use --dont-change-password for msktutil > 1.1

* Thu Aug 13 2020 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-11
- adapt samba workaround for later versions of samba, include C8 support

* Fri Nov 29 2019 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1.5-10
- Require perl(Term::ReadKey)

* Thu Nov 28 2019 Steve Traylen <steve.traylen@cern.ch> - 1.1.5-9
- Require perl(XML::Simple) explicitly

* Wed Oct 23 2019 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1.5-8
- added missing BuildRequires to be able to build on el8

* Wed Oct 02 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-7
- fix "Possible precedence issue" on perl 5.20+ (el8)

* Thu Aug 22 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-6
- do not try to set selinux fcontext if selinux is disabled (on el6)

* Fri Aug 16 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-5
- do not try to set selinux fcontext if selinux is disabled

* Thu Aug 15 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-4
- do not try to set selinux fcontext on remote filesystems

* Mon Jul 22 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-3
- add fix for if a https_proxy is set

* Wed Jul 03 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.1.5-2
- add missing requires for /usr/bin/tdbtool

* Fri Sep 21 2018 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1.5-1
- added (cc7/samba-4.7.X) workaround for setting SAMBA password.

* Tue Jun 12 2018 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1.4
- make it work for both aliases in "Dynamic Domains" and "Computers"

* Wed Jun 06 2018 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1.3
- added --testsrv option
* Thu Jun 22 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1
- implement dns alias keytabs generation.
* Thu Mar  9 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.0.4
- fix msktutil command line build for aliases.

* Thu Feb 16 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.0.3
- use --dont-update-dnshostname option

* Wed Feb 15 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.0.2
- fix undef in usenocanon()

* Mon Feb 13 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.0.1
- add user keytab generation

* Tue Apr 26 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.10-1
- fix enctypes processing

* Tue Dec 01 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.9-1
- fix --alias msktutil options.

* Mon Feb 16 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.8-2
- fix undef variable check.

* Fri Feb 13 2015 Thomas Oulevey <thomas.oulevey@cern.ch> 0.9.8
- workaround to not bail out for service ticket request after dyndns check.

* Wed Jan 28 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.6
- workaround to make it work for XXX.dyndns.cern.ch

* Tue Sep 02 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.5
- added cern-merge-keytab to the package.

* Fri Feb 07 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.4
- adding management of samba secrets.tdb
- change of SOAP method to reset passwd.

* Thu Feb 06 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.3
- adding time param. to ResetPass request

* Fri Mar 15 2013 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.8
- rewrite of code , to use single KDC.

* Tue Nov 27 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.5.3-1
- update requirement for msktutil >= 0.4.2
- and CERN-CA-certs for 5/6.

* Mon Nov 19 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.5-1
- using upstream msktutil now.

* Thu Oct 25 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.0.5-2
- production service SOAP URLs
* Wed Oct 24 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.0.4-1
- version using msktutil-cern.

* Tue Oct 16 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.0.2-1
- initial test release.
