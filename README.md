# cern-get-keytab

* cern-get-keytab is a utility supported by the linux team which allows a host to communicate to a web service (lxkerbwin, managed by IT-CD-DPP) to request a Kerberos keytab.
* Many tools at CERN rely on this utility and it is installed on the majority of hosts.  This package is provided through the 'CERN' repository on each supported distribution.

* This repository is configured for [rpmci](https://gitlab.cern.ch/linuxsupport/rpmci)
* Please see the [Kerberos](https://linuxops.web.cern.ch/cheatsheets/kerberos/) page for more details on Kerberos at CERN.
