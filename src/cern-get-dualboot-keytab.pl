#!/usr/bin/perl -w
#
# Get from Active Directory and store host Kerberos keytab and password
#
# 2012-10-20 v.0.3 Jaroslaw.Polok@cern.ch
#                    - initial version.
#
use strict;
use XML::Simple;
use WWW::Curl::Easy qw(/^CURLOPT_/ /^CURLINFO_/);
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
# use Digest::SHA qw(sha256_hex); # nope, not in SLC.
use MIME::Base64 qw(decode_base64);

use constant VERSION                    => '0.0.5';
use constant CERN_GET_KEYTAB_USER_AGENT => 'cern-get-dualboot-keytab/'
  . VERSION;
use constant CERN_KRB_KEYTAB_SRV => 'https://lxkerbwin.cern.ch/LxKerb.asmx';
use constant CERN_KRB_KEYTAB_LOCAL_PORT => '600';
use constant CERN_KRB_KEYTAB_DUALBOOT   => 'GetKeytabForDualBoot';
use constant KRB5_DEF_KEYTAB            => '/etc/krb5.keytab';
use constant KRB5_KEYTAB_PASSWD         => '.password';
use constant CHCON                      => '/usr/bin/chcon';
use constant CERN_CERT_CURL_CAPATH      => '/etc/pki/tls/certs/';
use constant CURLCERTINFO               => ( 0x400000 + 32 )
  ;    #not defined on SLC5 CURLINFO_CERTINFO

sub curlout_write_callback {
    my ( $chunk, $variable ) = @_;
    push @{$variable}, $chunk;
    return length($chunk);
}

sub errorout {
    my ( $msg, $code ) = @_;
    if ($code) {
        printf( "Error: " . $msg . "\n" );
    }
    else {
        printf( $msg. "\n" );
    }
    exit($code);
}

sub msg {
    my ($msg) = @_;
    printf( $msg. "\n" );
}

sub wrtfile {
    my ( $outfile, $outdata ) = @_;
    open( OUTF, '>:raw', $outfile ) || return 1;
    chmod 0600, $outfile;
    print OUTF $outdata;
    close(OUTF);
    return 0;
}

sub keytabfordualboot {
    my ( $keytabfile, $verbose, $debug ) = @_;
    my $curl = WWW::Curl::Easy->new();
    my $xml  = XML::Simple->new();
    my @curloutdata;

    $curl->setopt( CURLOPT_SSL_VERIFYPEER, 1 );    # CHANGE TO 1 FOR PROD !
    $curl->setopt( CURLOPT_SSL_VERIFYHOST, 1 );    # CHANGE TO 1 FOR PROD !
    $curl->setopt( CURLOPT_LOCALPORT,      CERN_KRB_KEYTAB_LOCAL_PORT );
    $curl->setopt( CURLOPT_LOCALPORTRANGE, 100 );
    $curl->setopt( CURLOPT_USERAGENT,      CERN_GET_KEYTAB_USER_AGENT );
    $curl->setopt( CURLOPT_FOLLOWLOCATION, 1 );
    $curl->setopt( CURLOPT_CONNECTTIMEOUT, 10 );
    $curl->setopt( CURLOPT_WRITEFUNCTION,  \&curlout_write_callback );
    $curl->setopt( CURLOPT_FILE,           \@curloutdata );
    $curl->setopt( CURLOPT_TIMEOUT,        60 );
    $curl->setopt( CURLOPT_VERBOSE,        1 ) if ($debug);

    # not everybody has certsd imported in NSS db
    # and we do not include CERN certs in ca-bundle.crt ...
    $curl->setopt( CURLOPT_CAPATH, CERN_CERT_CURL_CAPATH );

    $curl->setopt( CURLOPT_URL,
        CERN_KRB_KEYTAB_SRV . "/" . CERN_KRB_KEYTAB_DUALBOOT );
    msg( "curl GET " . CERN_KRB_KEYTAB_SRV . "/" . CERN_KRB_KEYTAB_DUALBOOT )
      if ($verbose);

    errorout(
        "cannot reset host password ("
          . $curl->errbuf()
          . " [http err:"
          . $curl->getinfo(CURLINFO_HTTP_CODE) . "])",
        1
    ) if ( $curl->perform() != 0 );

    errorout(
        "server error: "
          . $curl->getinfo(CURLINFO_HTTP_CODE)
          . ", server output:\n"
          . join( "", @curloutdata ) . "\n",
        1
    ) if ( $curl->getinfo(CURLINFO_HTTP_CODE) != 200 );

    my $outdata = $xml->XMLin( join( "", @curloutdata ) );

    printf Dumper($outdata) if ($debug);

    if ($verbose) {
        printf("curl RCV data:\n");
        printf( "success code:   " . $outdata->{success} . "\n" )
          if ( $outdata->{success} );
        printf( "error string:   " . $outdata->{error} . "\n" )
          if ( $outdata->{error} );
        printf( "computer. pass: " . $outdata->{computerpassword} . "\n" )
          if ( $outdata->{computerpassword} );
        printf( "samaccountname: " . $outdata->{samaccountname} . "\n" )
          if ( $outdata->{samaccountname} );
        printf( "principalname:  " . $outdata->{principalname} . "\n" )
          if ( $outdata->{principalname} );
        printf( "checksumsha256: " . $outdata->{checksumsha256} . "\n" )
          if ( $outdata->{checksumsha256} );
        printf( "Base64 keytab:\n" . $outdata->{keytab} . "\n" )
          if ( $outdata->{keytab} );
    }

    errorout( "cannot receive/parse server data:" . $outdata->{error}, 1 )
      if ( $outdata->{success} !~ /^true$/ );
    #
    # XML::Lite returns HASH for empty elements ? ...
    #
    errorout( "incomplete keytab data received.", 1 )
      if ( ( ref( $outdata->{keytab} ) eq "HASH" )
        || ( ref( $outdata->{checksumsha256} ) eq "HASH" )
        || ( ref( $outdata->{principalname} ) eq "HASH" )
        || ( ref( $outdata->{computerpassword} ) eq "HASH" )
        || ( ref( $outdata->{keytab} ) eq "HASH" ) );

    errorout( "incomplete keytab data received.", 1 )
      if ( !length( $outdata->{computerpassword} ) );

    my $keytabout = decode_base64( $outdata->{keytab} );

# this will just pull us an additional dependency on perl-Digest-SHA packages which are not in SLC.
#
# my $sha256sum=uc(sha256_hex($keytabout));
# errorout("cannot verify decoded keytab data") if ($outdata->{checksumsha256} !~ /^$sha256sum$/);

    msg("(over)writing keytab file: $keytabfile") if ($verbose);

    !wrtfile( $keytabfile, $keytabout )
      || errorout( "cannot open keytab file for writing ($keytabfile)", 1 );
    !wrtfile( $keytabfile . KRB5_KEYTAB_PASSWD, $outdata->{computerpassword} )
      || errorout(
        "cannot open keytab password file for writing ($keytabfile"
          . KRB5_KEYTAB_PASSWD . ")",
        1
      );
}

sub fixselinux {

    # would be nicer with some SE linux perl module...
    my ($keytabfile) = @_;
    my $devicenumber = ( stat($keytabfile) )[0];

    # only set the file context if the filesystem is local
    if (   ( $devicenumber > 0 && $devicenumber != 43 )
        && ( -e '/sys/fs/selinux' || -e '/selinux/status' ) )
    {
        system( CHCON. " system_u:object_r:krb5_keytab_t:s0 $keytabfile" )
          if ( -x CHCON && -f $keytabfile );
    }
}

my ( $debug, $verbose, $force, $keytab, $help, $service, $password ) = undef;

my %opts = (
    "debug"    => \$debug,
    "verbose"  => \$verbose,
    "keytab=s" => \$keytab,
    "force"    => \$force,
    "help"     => \$help,
);

errorout( "invalid options specified.", 1 ) if ( GetOptions(%opts) ne 1 );

pod2usage( -verbose => 2 ) if ($help);

errorout( "you must be 'root' to run this program.", 1 )
  if ( ( getpwuid $> ) ne 'root' );

$keytab = KRB5_DEF_KEYTAB if ( !defined($keytab) );

errorout(
    "Current keytab file ("
      . $keytab
      . ") already exists.\nUse --force to reinitialize.\nNOTE: This will cause reset of computer account in Active Directory,\nand your dual-boot MS Windows installation will not be able\nNOTE: to join CERN domain anymore, until the following procedure is applied:\nNOTE: https://twiki.cern.ch/twiki/bin/view/AFSService/MigrationFAQ",
    0
) if ( ( -f $keytab ) && ( !$force ) );

keytabfordualboot( $keytab, $verbose, $debug );

fixselinux($keytab);

msg("Keytab file saved: $keytab")                          if ($verbose);
msg( "Password file saved: $keytab" . KRB5_KEYTAB_PASSWD ) if ($verbose);
errorout(
"Please apply following procedure to enable your dual-boot MS Windows\ninstallation to join CERN domain:\nhttps://twiki.cern.ch/twiki/bin/view/AFSService/MigrationFAQ\n",
    0
);

__END__

=pod

=head1 NAME

cern-get-dualboot-keytab - utility to acquire and store CERN host keytab for dual-boot systems.

=head1 DESCRIPTION

cern-get-dualboot-keytab retrieves from CERN Active Directory Kerberos host identity and computer account password
and stores these in local keytab file: /etc/krb5.keytab and /etc/krb5.keytab.password.
Stored password can be used to allow dual-boot MS Windows installation to join CERN domain, applying
the procedure documented at:  https://twiki.cern.ch/twiki/bin/view/AFSService/MigrationFAQ

=head1 SYNOPSIS

=over 2

        cern-get-dualboot-keytab   [--help]

        cern-get-dualboot-keytab   [--keytab FILENAME][--force][--verbose][--debug]

=back

=head1 OPTIONS

=over 4

=item B<--help>

Shows this help description

=item B<--force>

Overwrites already existing host/service keytab file

=item B<--keytab FILENAME>

Stores retrieved keytab in FILENAME rather than default /etc/krb5.keytab

=back

=head1 EXAMPLES

=over 2

        cern-get-dualboot-keytab

=back

=head1 AUTHOR

Jaroslaw Polok <Jaroslaw.Polok@cern.ch>

=head1 NOTES

This utility should be used only for acquiring keytab for a dual-boot system,
For all other systems please use cern-get-keytab.


=head1 BUGS

??


