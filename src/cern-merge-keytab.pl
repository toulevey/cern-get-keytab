#!/usr/bin/perl -w
#
# Merge Kerberos keytab
#
# 2014-09-02 v.0.1  Jaroslaw.Polok@cern.ch
#                    - initial version
#
use strict;
use Getopt::Long;
use Pod::Usage;
use Authen::Krb5;
use Data::Dumper;
use File::Temp;

use constant VERSION                 => '0.1';
use constant KRB5_CCACHE_NAME        => 'MEMORY:cgk.' . $$;
use constant CHCON                   => '/usr/bin/chcon';
use constant ARCFOUR_HMAC            => 0x17;
use constant AES128_CTS_HMAC_SHA1_96 => 0x11;
use constant AES256_CTS_HMAC_SHA1_96 => 0x12;

sub errorout {
    my ( $msg, $code ) = @_;
    if ($code) {
        printf( "Error: " . $msg . "\n" );
    }
    else {
        printf( $msg. "\n" );
    }
    exit($code);
}

sub msg {
    my ($msg) = @_;
    printf( $msg. "\n" );
}

sub uniq {
    my %seen;
    return grep { !$seen{$_}++ } @_;
}

sub checkenctypes {
    my (@enctypes)    = @_;
    my (@enctypesout) = ();

    foreach my $enctin (@enctypes) {
        if ( $enctin =~ /^ARCFOUR_HMAC$/ ) {
            push( @enctypesout, ARCFOUR_HMAC );
            next;
        }
        if ( $enctin =~ /^AES128_CTS_HMAC_SHA1_96$/ ) {
            push( @enctypesout, AES128_CTS_HMAC_SHA1_96 );
            next;
        }
        if ( $enctin =~ /^AES256_CTS_HMAC_SHA1_96$/ ) {
            push( @enctypesout, AES256_CTS_HMAC_SHA1_96 );
            next;
        }
        errorout(
"Unknown enctype specified. Allowed ones are: ARCFOUR_HMAC,AES128_CTS_HMAC_SHA1_96,AES256_CTS_HMAC_SHA1_96",
            1
        );
    }
    return @enctypesout;
}

sub krb5init {
    my ( $verbose, $debug ) = @_;
    Authen::Krb5::init_context()
      or errorout( Authen::Krb5::error() . " while initializing context.", 1 );
    Authen::Krb5::init_ets()
      or errorout( Authen::Krb5::error() . " while initializing error tables.",
        1 );
}

sub krb5ccache {
    my ( $ccachefile, $verbose, $debug ) = @_;
    if ( defined($ccachefile) ) {
        msg(" resolving credentials cache: $ccachefile") if ($verbose);
        return Authen::Krb5::cc_resolve($ccachefile)
          || errorout(
            Authen::Krb5::error() . " while resolving ccache: $ccachefile", 1 );
    }
    msg(" resolving default credentials cache.") if ($verbose);
    return Authen::Krb5::cc_default()
      || errorout( Authen::Krb5::error() . " while resolving default ccache",
        1 );
}

sub krb5ktread {
    my ( $keytabfile, $verbose, $debug ) = @_;
    my (
        $krb5keytab, $krb5ktentry, $krb5ktcursor,
        $krb5princ,  $krb5ccache,  @krb5ktentries
    ) = undef;

    msg(" using keytab file name: $keytabfile") if ($verbose);

    errorout( " keytab file not readable: $keytabfile", 1 )
      if ( !-r $keytabfile );

    $krb5ccache = krb5ccache( KRB5_CCACHE_NAME, $verbose, $debug );

    msg(" resolving keytab file: $keytabfile") if ($verbose);
    $krb5keytab = Authen::Krb5::kt_resolve($keytabfile)
      or
      errorout( Authen::Krb5::error() . " while resolving keytab: $keytabfile",
        1 );

    $krb5ktcursor = $krb5keytab->start_seq_get();
    while ( $krb5ktentry = $krb5keytab->next_entry($krb5ktcursor) ) {
        msg( " found keytab entry for principal: "
              . join( "/", $krb5ktentry->principal->data ) )
          if ($debug);
        push( @krb5ktentries, $krb5ktentry );
    }
    $krb5keytab->end_seq_get($krb5ktcursor);

    return ( \@krb5ktentries );
}

sub krb5ktwrite {
    my ( $keytabfile, $krb5ktentries, $verbose, $debug ) = @_;
    my ( $krb5keytab, $krb5ktentry, $krb5ccache ) = undef;

    msg(" using keytab file name: $keytabfile") if ($verbose);

    $krb5ccache = krb5ccache( KRB5_CCACHE_NAME, $verbose, $debug );

    msg(" resolving keytab file $keytabfile") if ($verbose);
    $krb5keytab = Authen::Krb5::kt_resolve($keytabfile)
      or errorout(
        Authen::Krb5::error() . " while resolving keytab ($keytabfile).", 1 );

    foreach $krb5ktentry ( @{$krb5ktentries} ) {
        $krb5keytab->add_entry($krb5ktentry)
          or errorout(
            Authen::Krb5::error()
              . " while adding entry to keytab ($keytabfile)",
            1
          );
    }

}

sub krb5ktcleanup {
    my ( $krb5ktentries, $verbose, $debug ) = @_;
    my ( $krb5ktentry, @krb5ktentriesout, @principals, @enctypes,
        $krb5ktentrytouse, $kvno )
      = undef;

    @krb5ktentriesout = ();

    foreach $krb5ktentry ( @{$krb5ktentries} ) {
        push( @principals, join( "/", $krb5ktentry->principal->data ) );
        push( @enctypes,   $krb5ktentry->key->enctype );
    }

    foreach my $principal ( uniq(@principals) ) {
        foreach my $enctype ( uniq(@enctypes) ) {

            $kvno = 0;
            undef($krb5ktentrytouse);

            foreach $krb5ktentry ( @{$krb5ktentries} ) {
                next if ( $krb5ktentry->key->enctype != $enctype );
                my $tmp = join( "/", $krb5ktentry->principal->data );
                next if ( $principal ne $tmp );
                if ( $krb5ktentry->kvno > $kvno ) {
                    $kvno             = $krb5ktentry->kvno;
                    $krb5ktentrytouse = $krb5ktentry;
                }
            }
            push( @krb5ktentriesout, $krb5ktentrytouse );
        }
    }
    return ( \@krb5ktentriesout );
}

sub krb5ktprincs {
    my ( $krb5ktentries, $principals, $verbose, $debug ) = @_;
    my ( @krb5ktentriesout, $krb5ktentry, $principal ) = undef;

    @krb5ktentriesout = ();

    foreach $principal ( @{$principals} ) {
        foreach $krb5ktentry ( @{$krb5ktentries} ) {
            my $tmp = join( "/", $krb5ktentry->principal->data );
            if ( $tmp eq $principal ) {
                push( @krb5ktentriesout, $krb5ktentry );
            }
        }
    }
    return ( \@krb5ktentriesout );
}

sub krb5ktencs {
    my ( $krb5ktentries, $enctypes, $verbose, $debug ) = @_;
    my ( @krb5ktentriesout, $krb5ktentry, $enctype ) = undef;

    @krb5ktentriesout = ();

    foreach $enctype ( @{$enctypes} ) {
        foreach $krb5ktentry ( @{$krb5ktentries} ) {
            if ( $krb5ktentry->key->enctype == $enctype ) {
                push( @krb5ktentriesout, $krb5ktentry );
            }
        }
    }

    return ( \@krb5ktentriesout );
}

sub fixselinux {

    # would be nicer with some SE linux perl module...
    my ($keytabfile) = @_;
    my $devicenumber = ( stat($keytabfile) )[0];

    # only set the file context if the filesystem is local
    if (   ( $devicenumber > 0 && $devicenumber != 43 )
        && ( -e '/sys/fs/selinux' || -e '/selinux/status' ) )
    {
        system( CHCON. " system_u:object_r:krb5_keytab_t:s0 $keytabfile" )
          if ( -x CHCON && -f $keytabfile );
    }
}

#
# main() ;-)
#

my (
    $debug,      $verbose,  @inkeytabs, $outkeytab, $append, $cleanup,
    @principals, @enctypes, @enctenc,   $help,      @allentries
) = undef;

my %opts = (
    "debug"       => \$debug,
    "verbose"     => \$verbose,
    "inkeytab=s"  => \@inkeytabs,
    "outkeytab=s" => \$outkeytab,
    "append"      => \$append,
    "cleanup"     => \$cleanup,
    "principal=s" => \@principals,
    "enctype=s"   => \@enctypes,
    "help"        => \$help,
);

#@inkeytabs=split(/,/,join(',',@inkeytabs));

errorout( "invalid options specified.", 1 ) if ( GetOptions(%opts) ne 1 );

pod2usage( -verbose => 2 ) if ($help);

errorout( "input keytab file name(s) not specified.", 1 ) if ( !@inkeytabs );
errorout( "output keytab file name not specified.",   1 )
  if ( !defined($outkeytab) );
@enctenc = checkenctypes(@enctypes) if (@enctypes);

msg("Initializing Kerberos client") if ($verbose);
krb5init( $verbose, $debug );
foreach my $inkeytab (@inkeytabs) {
    msg("Reading input keytab: $inkeytab") if ($verbose);
    @allentries =
      ( @allentries, @{ krb5ktread( $inkeytab, $verbose, $debug ) } );
}

if ( defined($cleanup) ) {
    msg("Performing key entries cleanup") if ($verbose);
    @allentries = @{ krb5ktcleanup( \@allentries, $verbose, $debug ) };
}

if (@principals) {
    msg( "Matching selected principal(s): " . join( ",", @principals ) )
      if ($verbose);
    @allentries =
      @{ krb5ktprincs( \@allentries, \@principals, $verbose, $debug ) };
}

if (@enctypes) {
    msg( "Matching selected encoding type(s): " . join( ",", @enctypes ) )
      if ($verbose);
    @allentries = @{ krb5ktencs( \@allentries, \@enctenc, $verbose, $debug ) };
}

if ( -r $outkeytab && !defined($append) ) {
    msg("Removing existing keytab file: $outkeytab") if ($verbose);
    unlink($outkeytab)
      or errorout( "Cannot delete output keytab file: $outkeytab", 1 );
}

msg("Writing output keytab: $outkeytab") if ($verbose);
krb5ktwrite( $outkeytab, \@allentries, $verbose, $debug, );

msg("Fixing SELinux context") if ($verbose);
fixselinux($outkeytab);

exit(0);

__END__

=pod

=head1 NAME

cern-merge-keytab - utility to merge Kerberos keytabs.

=head1 DESCRIPTION

cern-merge-keytab collects keys from input keytabs and merges these into
a single output keytab.

=head1 SYNOPSIS

=over 2

        cern-merge-keytab   [--help]

        cern-merge-keytab   --inkeytab FILENAME --outkeytab FILENAME
                            [--append] [--cleanup]
                            [--principal PRINCIPAL]
                            [--enctype ENCTYPE]
			    [--verbose] [--debug]

=back

=head1 OPTIONS

=over 4

=item B<--help>

Shows this help description

=item B<--inkeytab FILENAME>

Input keytab file name. May be specified multiple times.

=item B<--outkeytab FILENAME>

Output keytab file name.

=item B<--append>

Append input keytab(s) keys to output keytab rather than overwriting it.

=item B<--cleanup>

Remove all but newest (highest kvno) input keytab(s) keys before writing to output keytab.

=item B<--principal PRINCIPAL>

Only act on input keytab(s) keys matching given Kerberos principal. May be specified multiple times.
(principals in CERN keytabs are defined as: 'hostname.cern.ch' , 'hostname$' or 'SRVC/hostname.cern.ch')

=item B<--enctype ENCTYPE>

Only act on input keytab(s) keys matching given encryption type(s). May be specified multiple times.
Allowed encryption types are: ARCFOUR_HMAC,AES128_CTS_HMAC_SHA1_96,AES256_CTS_HMAC_SHA1_96

=item B<--verbose>

Provide more information on the output.

=item B<--debug>

Provide detailed debugging information

=back

All options can be abbreviated to shortest distinctive lenght.  Single minus preceding option name may be used  instead of double one.

=head1 EXIT STATUS

=over 4

=item B<0> - on success.


=item B<1> - on error.

=back

=head1 EXAMPLES

=over 2

        cern-merge-keytab

        cern-merge-keytab --inkeytab /etc/keytab1.kt --inkeytab /etc/keytab2.kt --outkeytab /etc/keytab3.kt

        cern-merge-keytab -i /etc/kt1.kt -i /etc/kt2.kt -o /etc/kt3.kt -c -p SRVC/hostname.cern.ch -e AES256_CTS_HMAC_SHA1_96

=back

=head1 AUTHOR

Jaroslaw Polok <Jaroslaw.Polok@cern.ch>

=head1 BUGS

??

