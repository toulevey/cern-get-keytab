#!/usr/bin/perl -w
#
# Get from Active Directory and store host/service Kerberos keytab(s)
#
# 2022-06-07 v 1.1.6 Ulrich.Schwickerath@cern.ch
#                    - add support for cnames
#                    - add configuration file for lxkerbwin config
# 2018-06-12 v 1.1.5 Jaroslaw.Polok@cern.ch
#                    - (broken) samba 4.7 workaround
# 2018-06-12 v 1.1.4 Jaroslaw.Polok@cern.ch
#                    - work for aliases in "Dynamic Domains" and "Computers"
# 2018-06-06 v 1.1.3 Jaroslaw.Polok@cern.ch
#                    - add --testsrv option
# 2017-06-22 v 1.1   Jaroslaw.Polok@cern.ch
#                    - DNS alias keytab generation added.
# 2017-02-13 v 1.0.0  Jaroslaw.Polok@cern.ch
#                    - add user keytab generation
#                      (requires patched msktutil >= 1.0)
# 2016-09-02 v 0.9.11 Jaroslaw.Polok@cern.ch
#                    - add '--hostname' for dyndns hosts.
# 2016-04-26 v 0.9.10 Jaroslaw.Polok@cern.ch
#                    - fix enctypes processing
# 2015-12-01 v 0.9.9 Jaroslaw.Polok@cern.ch
#                    - fix --alias msktutil handling
# 2015-02-16 v 0.9.8 Jaroslaw.Polok@cern.ch
#                    - fix undefined var in dyndns code
# 2015-02-04 v 0.9.7 Jaroslaw.Polok@cern.ch
#                    - added --leavekrb5cfg option
# 2015-01-28 v 0.9.6 Jaroslaw.Polok@cern.ch
#                    - make it work for hostname.dyndns.cern.ch domain.
#                    - requires msktutil 0.5.1 + patch:
# 2014-09-02 v 0.9.5 Jaroslaw.Polok@cern.ch
#                    - cern-merge-keytab
# 2014-02-07 v 0.9.4 Jaroslaw.Polok@cern.ch
#                    - samba secrets mgmt, SOAP method change.
# 2014-02-06 v 0.9.3 Jaroslaw.Polok@cern.ch
#                    - adding timeparam to ResetPass request
# 2013-11-06 v 0.9.2 Jaroslaw.Polok@cern.ch
#                    - fix command line parsing
# 2013-10-17 v 0.9.1 Jaroslaw.Polok@cern.ch
#                    - use keytab auth for Dynamic DNS too.
# 2013-10-14 v 0.9   Jaroslaw.Polok@cern.ch
#                    - add handling of Dynamic DNS aliases
#                    - add remove option
# 2013-09-03 v.0.8.2 Jaroslaw.Polok@cern.ch
#                    - enforce default base for msktutil ldap
# 2013-03-15 v.0.8   Jaroslaw.Polok@cern.ch
#		      - contact only a single DC server.
# 2013-02-07 v.0.7.3 Jaroslaw.Polok@cern.ch
#                     - enctypes option
# 2013-02-04 v.0.7.1 Jaroslaw.Polok@cern.ch
#                     - use 'host/hostname...' for initial keytab validity test.
#                     - do not force regen of keytabs created using old system
# 2012-11-21 v.0.5.2 Jaroslaw.Polok@cern.ch
#                    - proper libcurl SSL verification
# 2012-11-18 v.0.5.1 Jaroslaw.Polok@cern.ch
#                    - rewrite to use upstream (GIT) msktutil
#
# 2012-10-20 v.0.0.3 Jaroslaw.Polok@cern.ch
#                    - initial version with (modified) msktutil.
#
use strict;
use XML::Simple;
use WWW::Curl::Easy qw(/^CURLOPT_/ /^CURLINFO_/);
use Getopt::Long;
use Pod::Usage;
use Authen::Krb5;
use Data::Dumper;
use File::Temp;
use Socket qw(AF_INET);
use POSIX qw(strftime);
use Term::ReadKey;

use constant {
    CERN_GET_KEYTAB_USER_AGENT => 'cern-get-keytab/1.1.6',
    CERN_KRB_KEYTAB_SRV        => 'https://lxkerbwin.cern.ch/LxKerb.asmx',
    CERN_KRB_KEYTAB_SRV_DEV    => 'https://lxkerbwindev.cern.ch/LxKerb.asmx',
    CERN_KRB_KEYTAB_LOCAL_PORT => '600',
    CERN_KRB_KEYTAB_RESETPWD   => 'ResetComputerPasswordTimeCheck',
    CERN_KRB_KEYTAB_RESETPWD_DNS_ALIAS =>
      'ResetComputerPasswordTimeCheckDnsAlias',
    CERN_KRB_KEYTAB_RESETPWD_CNAME =>
      'ResetComputerPasswordTimeCheckCName',
    MSKTUTIL         => '/usr/sbin/msktutil',
    SAMBAFIX         => '/usr/libexec/cern-get-keytab-samba-workaround',
    KRB5_DEF_KEYTAB  => '/etc/krb5.keytab',
    KRB5_CCACHE_NAME => 'MEMORY:cgk.' . $$,
    KRB5_CCACHE      => 'KRB5CCNAME=MEMORY:cgk.' . $$,
    MSKTUTILRUN      => '/usr/sbin/msktutil --update --dont-expire-password',
    LDAPBASEHOST     => '--base OU=Computers',
    LDAPBASEDNSALIAS =>
      '--base "DC=cern,DC=ch"',    # both Dynamic Domains and Computers ...
    LDAPBASEUSER         => '--base OU=Users',
    MSKTUTILRUN_DNSALIAS =>
'/usr/sbin/msktutil --update --dont-expire-password --no-canonical-name --dont-update-dnshostname',
    MSKTUTIL_NOCANON      => '--no-canonical-name',
    CHCON                 => '/usr/bin/chcon',
    CERN_CERT_CURL_CAPATH => '/etc/pki/tls/certs/',
    CURLCERTINFO => ( 0x400000 + 32 ),    #not defined on SLC5 CURLINFO_CERTINFO
    KDCNAME      => 'cerndc.cern.ch',
    KDCQANAME    => 'cerndcqa.cern.ch',
    REALMNAME    => 'CERN.CH',
    RC4_HMAC_MD5 => 0x04,
    AES128_CTS_HMAC_SHA1 => 0x08,
    AES256_CTS_HMAC_SHA1 => 0x10,
    DYNDNS_SUFFIX        => '.dyndns.cern.ch',
    MSKTUTIL_HOSTNAME    => '--hostname',
    DOMAIN_SUFFIX        => '.cern.ch',
    KRB5CFGTMP           => '/tmp/cgk.krb5.conf',
};

my $keytab_server;
my $verify_peer;
my $verify_host;

sub read_config($){
    my $config_file = shift;
    my %config;
    
    if (open( my $fh, "<", $config_file)){
	while (my $line = <$fh>){
	    chomp $line;
	    if ($line =~ /^([\w\d\_]+)\s*:\s*([:\.\/\d\w\-\_\"\']+)/) {
		$config{$1} = $2;
	    }
	}
    } else {
	printf("No configuration file found. Using defaults.\n")
    }
    return %config;
}

sub curlout_write_callback {
    my ( $chunk, $variable ) = @_;
    push @{$variable}, $chunk;
    return length($chunk);
}

sub errorout {
    my ( $msg, $code ) = @_;
    if ($code) {
        printf( "Error: " . $msg . "\n" );
    }
    else {
        printf( $msg. "\n" );
    }
    exit($code);
}

sub warnout {
    my ( $msg, $code ) = @_;
    if ($code) {
        printf( "Warning: " . $msg . "\n" );
    }
    else {
        printf( $msg. "\n" );
    }
}

#
#   this timestamp is used on server-side to decline password reset
#   if clock skew is too big (> 300 secs) server will not reset password
#   since it could not be used to auth. on client anyway ..
#   timeformat: as in http://msdn.microsoft.com/en-us/library/zdtaw1bw%28v=vs.110%29.aspx
sub reqtimestamp {

    # 2014-02-06T10:50:56 (in GMT)
    return strftime( "%FT%T", gmtime() );
}

sub msg {
    my ($msg) = @_;
    printf( $msg. "\n" );
}

sub decenctypes {
    my ($enctypesin)  = @_;
    my (@enctina)     = split( '\|', $enctypesin );
    my ($enctypesout) = 0x0;
    foreach my $enctin (@enctina) {
        if ( $enctin =~ /^RC4_HMAC_MD5$/ || $enctin =~ /^RC4$/ ) {
            $enctypesout |= RC4_HMAC_MD5;
            next;
        }
        if ( $enctin =~ /^AES128_CTS_HMAC_SHA1$/ || $enctin =~ /^AES128$/ ) {
            $enctypesout |= AES128_CTS_HMAC_SHA1;
            next;
        }
        if ( $enctin =~ /^AES256_CTS_HMAC_SHA1$/ || $enctin =~ /^AES256$/ ) {
            $enctypesout |= AES256_CTS_HMAC_SHA1;
            next;
        }
        errorout(
"Unknown enctype specified. Allowed ones are: RC4_HMAC_MD5|AES128_CTS_HMAC_SHA1|AES256_CTS_HMAC_SHA1",
            1
        );
    }
    return $enctypesout;
}
#
# OK, this DNS lookups seem not necessary, but SASL/GSSAPI auth in some versions of
# msktutil fails if we go via a round-robin IP alias of the dc ...
# we also want to talk to single KDC in order to avoid the need to wait for data
# propagation.
#
sub getdcname {
    my ( $kdcserver, $verbose ) = @_;
    my ( @addrs,     $host )    = undef;
    ( undef, undef, undef, undef, @addrs ) = gethostbyname($kdcserver);
    errorout( "Cannot find Domain Controller IP (" . $kdcserver . ") in DNS",
        1 )
      if ( !defined( $addrs[0] ) );
    ( $host, undef, undef, undef, undef ) = gethostbyaddr( $addrs[0], AF_INET );
    errorout(
        "Cannot find Domain Controller name ("
          . $kdcserver . " - "
          . $addrs[0]
          . ") in DNS",
        1
    ) if ( !defined($host) );
    msg("Found KDC: $host") if ($verbose);
    return $host;
}

# for XXX.dyndns.cern.ch
sub usenocanon {
    my ( $host, $verbose ) = @_;
    my ($name) = undef;
    ( $name, undef, undef, undef, undef ) = gethostbyname($host);

#errorout("Cannot find this host hostname (".$host.") in DNS",1) if (!defined($name));
    if ( defined($name) && $name =~ /(.*)${\(DYNDNS_SUFFIX)}$/ ) {
        return
            " "
          . MSKTUTIL_NOCANON . " "
          . MSKTUTIL_HOSTNAME . " $1"
          . DOMAIN_SUFFIX;
    }
    return "";
}

sub resetpass {
    my ( $service, $isolate, $interface, $verbose, $alias, $cname, $debug, $testsrv ) =
      @_;
    my $curl = WWW::Curl::Easy->new();
    my $xml  = XML::Simple->new();
    my @curloutdata;
    my $curlurl = $keytab_server . "/";

    $curl->setopt( CURLOPT_SSL_VERIFYPEER, $verify_peer ); # USE 1 FOR PROD !
    $curl->setopt( CURLOPT_SSL_VERIFYHOST, $verify_host ); # USE 2 FOR PROD !

    if ($testsrv) {
        $curlurl = CERN_KRB_KEYTAB_SRV_DEV . "/";
        $curl->setopt( CURLOPT_SSL_VERIFYPEER, 0 );
        $curl->setopt( CURLOPT_SSL_VERIFYHOST, 0 );
    }

    $curl->setopt( CURLOPT_LOCALPORT,      CERN_KRB_KEYTAB_LOCAL_PORT );
    $curl->setopt( CURLOPT_LOCALPORTRANGE, 100 );
    $curl->setopt( CURLOPT_USERAGENT,      CERN_GET_KEYTAB_USER_AGENT );
    $curl->setopt( CURLOPT_FOLLOWLOCATION, 1 );
    $curl->setopt( CURLOPT_CONNECTTIMEOUT, 10 );
    $curl->setopt( CURLOPT_WRITEFUNCTION,  \&curlout_write_callback );
    $curl->setopt( CURLOPT_FILE,           \@curloutdata );
    $curl->setopt( CURLOPT_TIMEOUT,        60 );
    $curl->setopt( CURLOPT_INTERFACE, $interface ) if ( defined($interface) );
    msg( "curl interface set to: " . $interface )  if ( defined($interface) );
    $curl->setopt( CURLOPT_VERBOSE, 1 )            if ($debug);

    # not everybody has certsd imported in NSS db
    # and we do not include CERN certs in ca-bundle.crt ...
    $curl->setopt( CURLOPT_CAPATH, CERN_CERT_CURL_CAPATH );

    $service = "host" if ( !defined($isolate) or !defined($service) );

    if ($alias) {
        $curlurl .= CERN_KRB_KEYTAB_RESETPWD_DNS_ALIAS;
    } elsif ($cname) {
        $curlurl .= CERN_KRB_KEYTAB_RESETPWD_CNAME;
    }
    else {
        $curlurl .= CERN_KRB_KEYTAB_RESETPWD;
    }

    $curlurl .= "?service=$service";

    if ($alias) {
        $curlurl .= "&dnsalias=$alias";
    }

    if ($cname) {
        $curlurl .= "&cname=$cname";
    }

    $curlurl .= "&t=" . reqtimestamp();

    if ( exists( $ENV{"HTTPS_PROXY"} ) || exists( $ENV{"https_proxy"} ) ) {
        my ($kerbhost) = $curlurl =~ m|^( .*?\. [^/]+ )|x;
        $kerbhost =~ s|^https://||;
        msg(
"https proxy detected, instructing call to $kerbhost to ignore the configured proxy"
        ) if ($verbose);
        $curl->setopt( CURLOPT_NOPROXY, $kerbhost );
    }

    $curl->setopt( CURLOPT_URL, $curlurl );
    msg( "curl GET " . $curlurl ) if ($verbose);

    errorout(
        "cannot reset host password ("
          . $curl->errbuf()
          . " [http err:"
          . $curl->getinfo(CURLINFO_HTTP_CODE) . "])",
        1
    ) if ( $curl->perform() != 0 );

    errorout(
        "server error: "
          . $curl->getinfo(CURLINFO_HTTP_CODE)
          . ", server output:\n"
          . join( "", @curloutdata ) . "\n",
        1
    ) if ( $curl->getinfo(CURLINFO_HTTP_CODE) != 200 );

    my $outdata = $xml->XMLin( join( "", @curloutdata ) );

    printf Dumper($outdata) if ($debug);

    if ($verbose) {
        printf("curl RCV data:\n");
        printf( "success code:   " . $outdata->{success} . "\n" )
          if ( $outdata->{success} );
        printf( "error string:   " . $outdata->{error} . "\n" )
          if ( $outdata->{error} );
        printf( "computer. pass: " . $outdata->{computerpassword} . "\n" )
          if ( $outdata->{computerpassword} );
        printf( "samaccountname: " . $outdata->{samaccountname} . "\n" )
          if ( $outdata->{samaccountname} );
        printf( "principalname:  " . $outdata->{principalname} . "\n" )
          if ( $outdata->{principalname} );
    }

    errorout( "cannot receive/parse server data:" . $outdata->{error}, 1 )
      if ( $outdata->{success} !~ /^true$/ );

    #
    # XML::Lite returns HASH for empty elements ? ...
    #
    errorout( "incomplete data received.", 1 )
      if ( ( ref( $outdata->{samaccountname} ) eq "HASH" )
        || ( ref( $outdata->{computerpassword} ) eq "HASH" ) );

    return ( $outdata->{samaccountname}, $outdata->{computerpassword} );
}

sub krb5cfgfile {
    my ( $kdc, $realm, $leavekrb5cfg, $verbose, $debug ) = @_;
    my ( $tfh, $tfname ) = undef;
    if ($leavekrb5cfg) {
        $tfname = KRB5CFGTMP;
        unlink($tfname) if ( -r $tfname );
        open( $tfh, ">", $tfname )
          or errorout( "cannot create temporary krb5 config file: $tfname", 1 );
    }
    else {
        ( $tfh, $tfname ) = File::Temp::tempfile(
            "cgk.krb5.conf.XXXXXX",
            TMPDIR => 1,
            UNLINK => 1
          )
          or errorout( "cannot create temporary krb5 config file: $tfname", 1 );
    }
    printf $tfh "[libdefaults]\ndefault_realm="
      . $realm
      . "\ndns_lookup_kdc=false\nforwardable=true\nproxiable=true\n"
      or errorout( "cannot write temp. krb5 config file: $tfname", 1 );
    printf $tfh "[realms]\n"
      . $realm
      . "={\nkdc="
      . $kdc
      . "\nadmin_server="
      . $kdc . "\n}\n"
      or errorout( "cannot write temp. krb5 config file: $tfname", 1 );
    $tfh->flush();
    $ENV{'KRB5_CONFIG'} = $tfname;
    msg("created temporary krb5 config file: $tfname") if ($debug);
}

sub krb5init {
    my ( $verbose, $debug ) = @_;
    Authen::Krb5::init_context()
      or errorout( Authen::Krb5::error() . " while initializing context.", 1 );
    Authen::Krb5::init_ets()
      or errorout( Authen::Krb5::error() . " while initializing error tables.",
        1 );
}

sub krb5ccache {
    my ( $ccachefile, $verbose, $debug ) = @_;
    if ( defined($ccachefile) ) {
        msg("resolving credentials cache ($ccachefile).") if ($verbose);
        return Authen::Krb5::cc_resolve($ccachefile)
          || errorout(
            Authen::Krb5::error() . " while resolving ccache ($ccachefile)",
            1 );
    }
    msg("resolving default credentials cache.") if ($verbose);
    return Authen::Krb5::cc_default()
      || errorout( Authen::Krb5::error() . " while resolving default ccache",
        1 );
}

#
# this function is not needed in most cases: we just keep it in
# - in case we would like to do some ccache entries parsing.
# in addition it does not really authenticate ...
#
sub krb5ccacheauth {
    my ( $ccachefile, $verbose, $debug ) = @_;
    my (
        $krb5ccache,   $krb5ccacheout, $krb5princ,
        $krb5cccursor, $krb5ccentry,   $tmpfn
    ) = undef;

    msg("using credentials cache: $ccachefile") if ($verbose);

    if   ( $ccachefile =~ /^FILE:(.*)/ ) { $tmpfn = $1; }
    else                                 { $tmpfn = $ccachefile; }

    errorout( "Kerberos credentials cache not readable.", 1 ) if ( !-r $tmpfn );

    $krb5ccache    = krb5ccache( $ccachefile,      $verbose, $debug );
    $krb5ccacheout = krb5ccache( KRB5_CCACHE_NAME, $verbose, $debug );

    msg("copying credentials cache content to temporary ccache") if ($verbose);

    $krb5princ = $krb5ccache->get_principal();

    msg( "found principal: " . $krb5princ->data ) if ($verbose);

    $krb5ccacheout->initialize($krb5princ);

    $krb5cccursor = $krb5ccache->start_seq_get();
    while ( $krb5ccentry = $krb5ccache->next_cred($krb5cccursor) ) {
        $krb5ccacheout->store_cred($krb5ccentry);
    }
    $krb5ccache->end_seq_get($krb5cccursor);

    msg("credentials cache copied") if ($verbose);
    return ( $krb5princ, $krb5ccacheout );
}

sub krb5ktauth {
    my ( $principal, $keytabfile, $ccachefile, $verbose, $debug ) = @_;
    my ( $krb5keytab, $krb5ktentry, $krb5ktcursor, $krb5princ, $krb5ccache ) =
      undef;

    msg("using default keytab file name.")
      if ( $verbose && !defined($keytabfile) );

    # this returns undef ... while shouldn't ..
    #$keytabfile=Authen::Krb5::kt_default_name() if (!defined($keytabfile));
    $keytabfile = KRB5_DEF_KEYTAB if ( !defined($keytabfile) );

    msg("using keytab file name: $keytabfile") if ($verbose);

    msg("keytab file not readable.") if ( $verbose && !-r $keytabfile );
    return ( undef, $keytabfile )    if ( !-r $keytabfile );

    $krb5ccache = krb5ccache( $ccachefile, $verbose, $debug );

    msg("resolving keytab file $keytabfile") if ($verbose);
    $krb5keytab = Authen::Krb5::kt_resolve($keytabfile)
      or errorout(
        Authen::Krb5::error() . " while resolving keytab ($keytabfile).", 1 );

    if ( !defined($principal) ) {
        msg("scanning keytab file for principal name") if ($verbose);
        $krb5ktcursor = $krb5keytab->start_seq_get();
        if ( !defined($krb5ktcursor) ) {
            msg("$keytabfile is corrupt, removing");
            unlink($keytabfile);
        }
        else {
            while ( $krb5ktentry = $krb5keytab->next_entry($krb5ktcursor) ) {
                if (
                    $krb5ktentry->principal->data =~ /\.*\$$/
                    ||    #hostname$@REALM MS principal
                    $krb5ktentry->principal->data =~ /^(.*)\.(.*)$/
                  )
                {         #host/hostname.domain.name@REALM principal
                    $principal = $krb5ktentry->principal->data;
                    $principal = "host/" . $principal
                      if ( $principal !~ /\$$/ );
                    msg("found principal name: $principal") if ($verbose);
                    last;
                }
            }
            $krb5keytab->end_seq_get($krb5ktcursor);
        }
        msg("principal name not found.")
          if ( $verbose && !defined($principal) );
        return ( undef, $keytabfile ) if ( !defined($principal) );
    }

    $krb5princ = Authen::Krb5::parse_name($principal)
      or
      errorout( Authen::Krb5::error() . " while parsing client principal.", 1 );
    $krb5ccache->initialize($krb5princ)
      or errorout( Authen::Krb5::error() . " while initalizing ccache.", 1 );
    msg(    "authenticating as: "
          . $principal
          . " using keytab file: "
          . $keytabfile
          . "." )
      if ($verbose);
    return ( $principal, $keytabfile )
      if ( Authen::Krb5::get_init_creds_keytab( $krb5princ, $krb5keytab ) );
    return ( undef, $keytabfile );
}

sub krb5pwauth {
    my ( $principal, $password, $ccachefile, $verbose, $debug ) = @_;
    my ( $krb5princ, $krb5ccache ) = undef;

    $krb5ccache = krb5ccache($ccachefile);
    $krb5princ  = Authen::Krb5::parse_name($principal)
      or
      errorout( Authen::Krb5::error() . " while parsing client principal.", 1 );
    $krb5ccache->initialize($krb5princ)
      or errorout( Authen::Krb5::error() . " while initalizing ccache.", 1 );
    msg(    "authenticating as "
          . $principal
          . " using password ("
          . $password
          . ")." )
      if ($verbose);
    return 1
      if ( Authen::Krb5::get_init_creds_password( $krb5princ, $password ) );
    return 0;
}

sub myexec {
    my ( $argstr, $verbose ) = @_;
    my $argstrprt = $argstr;
    $argstrprt =~
s/(.*)--old-account-password\s+\'(.*)\'(.*)$/$1--old-account-password ******** $3/;
    msg("executing $argstrprt") if ($verbose);
    if ( system($argstr) != 0 ) {
        if ( $? == -1 ) { errorout( "failed to execute.",               1 ); }
        if ( $? & 127 ) { errorout( "child exit code: " . ( $? & 127 ), 1 ); }
    }
    return 1;
}

sub fixselinux {

    # would be nicer with some SE linux perl module...
    my ($keytabfile) = @_;
    my $devicenumber = ( stat($keytabfile) )[0];

    # only set the file context if the filesystem is local
    if (   ( $devicenumber > 0 && $devicenumber != 43 )
        && ( -e '/sys/fs/selinux' || -e '/selinux/status' ) )
    {
        system( CHCON. " system_u:object_r:krb5_keytab_t:s0 $keytabfile" )
          if ( -x CHCON && -f $keytabfile );
    }
}

#
# main() ;-)
#

errorout( "msktutil not found. (" . MSKTUTIL . ").", 1 ) if ( !-x MSKTUTIL );

my (
    $debug,     $verbose,      $force,      $keytab,      $type,
    $alias,     $ccache,       $help,       $service,     $remove,
    $isolate,   $hostname,     $enctypes,   $decenctypes, $password,
    $principal, $runmsktutil,  $kdcserver,  $krealm,      $msktutilexec,
    $smbsecret, $leavekrb5cfg, $userkeytab, $userlogin,   $userpass,
    $testsrv, $cname
) = undef;

my %opts = (
    "debug"        => \$debug,
    "verbose"      => \$verbose,
    "keytab=s"     => \$keytab,
    "force"        => \$force,
    "service=s"    => \$service,
    "remove=s"     => \$remove,
    "isolate"      => \$isolate,
    "hostname=s"   => \$hostname,
    "enctypes=s"   => \$enctypes,
    "alias=s"      => \$alias,
    "cname=s"      => \$cname,
    "ccache=s"     => \$ccache,
    "leavekrb5cfg" => \$leavekrb5cfg,
    "help"         => \$help,
    "passwordsmb"  => \$smbsecret,
    "user"         => \$userkeytab,
    "login=s"      => \$userlogin,
    "password=s"   => \$userpass,
    "testsrv"      => \$testsrv,
);

errorout( "invalid options specified.", 1 ) if ( GetOptions(%opts) ne 1 );

pod2usage( -verbose => 2 ) if ($help);

errorout("you must be 'root' to run this program.", 1 ) if ( ( getpwuid $> ) ne 'root' ) && ( !defined($alias) && !defined($userkeytab) );
errorout("only one of --service or --remove can be used at the same time.", 1 ) if ( defined($service) && defined($remove) );
errorout("service name can contain only alphanumeric characters ([a-zA-Z0-9_-]).", 1 ) if ( defined($service) && $service !~ /^[a-zA-Z0-9_-]+$/ );
errorout("remove service name can contain only alphanumeric characters ([a-zA-Z0-9_-]).", 1) if ( defined($remove) && $remove !~ /^[a-zA-Z0-9_-]+$/ );
errorout("--isolate option must be used together with --service option.", 1 ) if ( !defined($service) && defined($isolate) );
errorout("--alias option must be used together with --keytab option.", 1 ) if ( defined($alias) && !defined($keytab) );
errorout("--cname option must be used together with --keytab option.", 1 ) if ( defined($cname)&& !defined($keytab) );
errorout("please specify either --alias option or --cname.", 1 ) if ( defined($alias) && defined($cname) );
errorout("--ccache option can be used only together with --cname or --alias option", 1 ) if ( defined($ccache) && !(defined($alias)||defined($cname)) );
errorout("--service, --isolate, --remove, --force, --hostname, --alias, --cname, --passwordsmb are mutually exclusive with --user option", 1 ) if (
    defined($userkeytab)
    && (   defined($service)
        || defined($isolate)
        || defined($remove)
        || defined($force)
        || defined($hostname)
        || defined($alias)
        || defined($cname)
        || defined($smbsecret) )
  );
errorout("--user option must be used together with --keytab option.", 1 ) if ( defined($userkeytab) && !defined($keytab) );
errorout("--password, --login options can be used only together with --user option.", 1) if ( ( defined($userpass) || defined($userlogin)) && !defined($userkeytab) );
warnout("--testsrv is deprecated. Please use a configuration file instead.", 1) if (defined($testsrv));

my $config_file = $ENV{"CERN_GET_KEYTAB_CONF"} || "/etc/cern-get-keytab.yaml";
my %config = read_config($config_file);

# initialise keytab server settings
$keytab_server = $config{"server"} || CERN_KRB_KEYTAB_SRV;
$verify_peer = $config{"verify_peer"} || "true";
$verify_host = $config{"verify_host"} || "true";
if (lc($verify_peer) eq "true") {$verify_peer = 1;} else {$verify_peer = 0;};
if (lc($verify_host) eq "true") {$verify_host = 2;} else {$verify_host = 0;};

if ( defined($smbsecret) ) {
    myexec( SAMBAFIX, $verbose );
}

if ( defined($enctypes) ) { $decenctypes = decenctypes($enctypes); }

if ( defined($isolate) && !defined($keytab) ) {
    $keytab = KRB5_DEF_KEYTAB . "." . $service;
    msg("using $keytab as keytab file");
}

# make sure both perl and msktutil will talk to the same KDC.
if ($keytab_server =~ /lxkerbwinqa\.cern\.ch/) {
    $kdcserver = getdcname( KDCQANAME, $verbose );
} else {
    $kdcserver = getdcname( KDCNAME, $verbose );
}
$krealm    = REALMNAME;

msg("Setting up temporary Kerberos config file") if ($verbose);
krb5cfgfile( $kdcserver, $krealm, $leavekrb5cfg, $verbose, $debug );

msg("Initializing Kerberos client") if ($verbose);
krb5init( $verbose, $debug );

# this assures that host/... SPN is created (since it is not there by default in AD)
$service = "host" if ( !defined($service) );

$runmsktutil = "";
if ($userkeytab) {
    $runmsktutil .= LDAPBASEUSER;
}
elsif ($alias) {
    $runmsktutil .= LDAPBASEDNSALIAS;
}
else {
    $runmsktutil .= LDAPBASEHOST;
}

$runmsktutil .= " --verbose --verbose" if ($debug);
$runmsktutil .= " --verbose"           if ($verbose);

# this option is available on patched msktutil-1.0 only ..
$runmsktutil .= " --dont-update-dnshostname" if ($isolate);

if ($remove) {
    $runmsktutil .= " --remove-service $remove";
}
else {
    if ( !defined($userkeytab) ) {
        $runmsktutil .= " --service $service" if ($service);
    }
}
$runmsktutil .= " --keytab $keytab"     if ($keytab);
$runmsktutil .= " --hostname $hostname" if ($hostname);

# msktutil can use _kerberos._tcp.cern.ch properly to find DCs ...
# but still we want to talk to a single KDC all the time.
$runmsktutil .= " --server $kdcserver";
$runmsktutil .= " --enctypes " . $decenctypes if ( defined($decenctypes) );

$runmsktutil .= " --set-samba-secret" if ( defined($smbsecret) );

if ( defined($alias) ) {
    $msktutilexec =
        KRB5_CCACHE . " "
      . MSKTUTILRUN_DNSALIAS . " "
      . MSKTUTIL_HOSTNAME . " "
      . $alias . " "
      . $runmsktutil;
} elsif (defined($cname)) {
    $msktutilexec =
        KRB5_CCACHE . " "
      . MSKTUTILRUN_DNSALIAS . " "
      . MSKTUTIL_HOSTNAME . " "
      . $cname . " "
      . $runmsktutil;
}
else {
    $msktutilexec = KRB5_CCACHE . " " . MSKTUTILRUN . " " . $runmsktutil;
}

if ( defined($userkeytab) ) {
    $msktutilexec .= " --dont-change-password --use-service-account";
}

if ( defined($userkeytab) && !defined($userlogin) ) {
    $msktutilexec .= " --account-name " . getpwuid $>;
}
elsif ( defined($userkeytab) ) {
    $msktutilexec .= " --account-name $userlogin";
}

if ( defined($userkeytab) && defined($userpass) ) {
    $msktutilexec .= " --old-account-password $userpass";
}
elsif ( defined($userkeytab) ) {
    ReadMode('noecho');

    printf "Password for ";
    if ( defined($userlogin) ) {
        printf "$userlogin:";
    }
    else {
        my $uname = getpwuid $>;
        printf "$uname:";
    }
    my $pass = ReadLine(0);
    chomp($pass);
    printf "\n";
    ReadMode('restore');
    $msktutilexec .= " --old-account-password '$pass'";
}

#
# If we already have a keytab, try to authenticate using it .. unless this is for user keytab
#

if ( !defined($userkeytab) ) {
    msg("Authenticating using keytab file.") if ($verbose);
    ( $principal, $keytab ) =
      krb5ktauth( undef, $keytab, "MEMORY:cgkccache" . $$, $verbose, $debug );
    if ( defined($principal) ) {
        msg("Successfully authenticated using keytab file") if ($verbose);
        errorout(
            "Current keytab file ("
              . $keytab
              . ") is valid.\nUse --force to reinitialize.",
            0
        ) if ( !$force );

# little bit cheating: keytab IS valid but we need computer-name == principal here ...
        if ( $principal !~ /^host\// ) {
            $principal =~ s/\$$//;
            myexec(
                $msktutilexec
                  . usenocanon($principal)
                  . " --computer-name "
                  . $principal,
                $verbose
            );
            fixselinux($keytab);
            errorout( "Keytab file saved: $keytab", 0 ) if ( -r $keytab );
        }
    }

    msg("Authentication using keytab file failed") if ( $verbose && !$force );
}

if ( !defined($userkeytab) ) {
    msg("Requesting password reset") if ($verbose);
    ( $principal, $password ) =
      resetpass( $service, $isolate, $hostname, $verbose, $alias, $cname, $debug,
        $testsrv);
    my $i = -1;
    while ( $i++ < 10 ) {
        printf( "Waiting for password replication (%d seconds past)\n",
            $i * 5 );
        msg("Authenticating using password (try $i/10)") if ($verbose);
        if (
            krb5pwauth(
                $principal, $password, "MEMORY:cgkccache2" . $$,
                $verbose,   $debug
            )
          )
        {
            $principal =~ s/\$$//;
            msg("Successfully authenticated using password") if ($verbose);
            myexec(
                $msktutilexec
                  . usenocanon($principal)
                  . " --computer-name "
                  . $principal
                  . " --old-account-password "
                  . $password,
                $verbose
            );
            fixselinux($keytab);
            errorout( "Keytab file saved: $keytab", 0 ) if ( -r $keytab );
        }
        sleep(5);
    }
    msg("Authentication using keytab file and password failed.") if ($verbose);
    errorout( "Cannot get keytab.", 1 );

    #
    # user keytab
    #

}
else {
    myexec( $msktutilexec, $verbose );
    fixselinux($keytab);
    errorout( "Keytab file generated: $keytab", 0 ) if ( -r $keytab );
    errorout( "Generating keytab failed.",      1 );
}

__END__

=pod

=head1 NAME

cern-get-keytab - utility to acquire and store CERN host/service or user Kerberos keytab.

=head1 DESCRIPTION

cern-get-keytab retrieves from CERN Active Directory Kerberos host/service identities and
stores these in a local keytab file. It can also generate user keytab.

=head1 SYNOPSIS

=over 2

        cern-get-keytab   [--help]

        cern-get-keytab   [--keytab FILENAME][--force][--verbose][--debug]
                          [--passwordsmb][--leavekrb5cfg]

        cern-get-keytab   [--keytab FILENAME][--service SRVC][--force]
                          [--isolate][--hostname HOSTNAME]
                          [--enctypes ENCTYPE|ENCTYPE|.. ]
                          [--passwordsmb][--leavekrb5cfg][--verbose][--debug]

        cern-get-keytab  --keytab FILENAME --alias ALIAS
                          [--service SRVC][--force]
                          [--leavekrb5cfg][--verbose][--debug]

        cern-get-keytab  --keytab FILENAME --cname ALIAS
                          [--service SRVC][--force]
                          [--leavekrb5cfg][--verbose][--debug]

        cern-get-keytab   [--keytab FILENAME][--force][--verbose][--debug]
                          [--isolate][--alias ALIAS][--service SERVICE]

        cern-get-keytab   [--keytab FILENAME][--force][--verbose][--debug]
                          [--isolate][--cname ALIAS][--service SERVICE]

        cern-get-keytab --user --keytab FILENAME
                          [--login LOGIN] [--password PASSWORD ]
                          [--leavekrb5cfg][--verbose][--debug]

=back

=head1 OPTIONS

=over 4

=item B<--help>

Shows this help description

=item B<--force>

Appends new to already existing valid host/service keytab file, invalidating current entries.

=item B<--service SRVC>

Acquires keytab for given service rather than host. SRVC name is case-sensitive
(example common service names: HTTP, cvs, ...). See also --isolate option.
If not specified the default service: host is used.

=item B<--remove SRVC>

Removes keys for named service from Active Directory.

=item B<--keytab FILENAME>

Stores retrieved keytab in FILENAME rather than default /etc/krb5.keytab

=item B<--isolate>

Active Directory provides keytabs for host and services using same encryption keys.
This option allows for creating service keytab with encyption keys different that host keytab
encryption keys for improved security. By default such keytabs are stored as /etc/krb5.keytab.SRVC
unless --keytab option is specified.

=item B<--hostname HOSTNAME>

Use HOSTNAME for requesting Active Directory password reset.
This option is to be used ONLY when system has multiple interfaces / ip addresses
allocated and keytab is required for hostname corresponding to a non-default route interface.

System network interface corresponding to HOSTNAME must be defined and active before
running cern-get-keytab with this option specified.

This option will not funciton while using DNS host aliases.
See next option.

=item B<--alias ALIAS>
=item B<--cname ALIAS>

Acquire keytab for CERN DNS Dynamic Alias (delegated zone/subdomain),
or Landb load balancing alias (*--load-X-)
This operation will only succeed if executed on a system
being active member of DNS alias.

=item B<--user>

Generate keytab for user account, this option requires specifying
user account password and optionally user login.

Such keytab can be used for authentication with kinit:

# kinit -kt /path/to/user.keytab.file login

WARNING: store user keytab in protected location: this file
contains your user credentials which can be used to authenticate
until account password is changed.

=item B<--password PASSWORD>

User account password for user keytab generation.
If not provided, will be asked interactively.
Special characters in password need to be shell-escaped.

=item B<--login LOGIN>

User account login for user keytab generation.
If not provided current username is used.

=item B<--enctypes ENCTYPE|ENCTYPE|..>

Specify encryption types to be used for obtained host identity.
Allowed (and supported on CERN Active Directory) ENCTYPEs are:

RC4_HMAC_MD5          (RC4)
AES128_CTS_HMAC_SHA1  (AES128)
AES256_CTS_HMAC_SHA1  (AES256)

Default ENCTYPEs are: RC4_HMAC_MD5|AES128_CTS_HMAC_SHA1|AES256_CTS_HMAC_SHA1

=item B<--passwordsmb>

Saves computer account password in Samba secrets database
(/var/lib/samba/private/secrets.tdb) for use with kerberized smbd setup.

=item B<--leavekrb5cfg>

Store used temporary kerberos config file as:

/tmp/cgk.krb5.conf

This can be used by another tool in order to guarantee that same
Active Directory Domain Controller will be used, therefore eliminating
the need to wait for (asynchronous) AD replication.

To use from (ba)sh shell:

export KRB5_CONFIG=/tmp/cgk.krb5.conf

Warning: if /tmp/cgk.krb5.conf exists its content will be overwritten.

=item B<--testsrv>

DEPRECATED. Please use a configuration file instead.
Use development instance of password reset server. DO NOT USE IN PRODUCTION.

=back

=head1 EXAMPLES

=over 2

        cern-get-keytab

        cern-get-keytab --service HTTP

        cern-get-keytab --service EXTRASECURE --isolate

        cern-get-keytab --alias dnsalias.cern.ch --keytab /etc/krb5.keytab.dnsalias

        cern-get-keytab --user --login LOGIN --keytab ~/private/login.keytab

=back

=head1 ENVIRONMENT

CERN_GET_KEYTAB_CONF
Full path to the configuration file. By defaults it is expected to be in /etc/cern-get-keytab.yaml.

=head1 Configuration
The configuration file should look like this:

C<
    server: https://lxkerbwindev.cern.ch/LxKerb.asmx
    verify_peer: false
    verify_host: false
>
The server field contains the full path to the endpoint.
The parameters verify_peer and verify_host determin if SSL verification should be performed, see CURLOPT_SSL_VERIFYPEER and CURLOPT_SSL_VERIFYHOST.
False is translated to zero, True is translated to 1 for CURLOPT_SSL_VERIFYPEER and 2 for CURLOPT_SSL_VERIFYHOST.


If no configuration file is found, production default values will be assumed.

=head1 AUTHOR

Jaroslaw Polok <Jaroslaw.Polok@cern.ch>

=head1 NOTES

All options can be abbreviated to their shortest distinctive length.

Creation and retrieval of a new keytab invalidates current one.
(except for user keytab)

=head1 BUGS

--alias option can be used for AD objects which are not DNS aliases

--remove removes principal correctly from AD but leaves it in keytab
         (rerun to get it cleared from keytab)

??
