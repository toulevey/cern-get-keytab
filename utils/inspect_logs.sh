#!/bin/bash
# This assumes the project includes a triggered job (imageci) ...
PROJECT_PATH="linuxsupport/rpms/cern-get-keytab"
IMAGECI_PATH="linuxsupport/testing/image-ci"
if [ $# -eq 0 ]; then
  BRANCH="master"
else
  BRANCH=$1
fi

### Should not need to change anything below here
TMPDIR=$(mktemp -d)
URL_ESCAPED_PROJECT_PATH=$(echo ${PROJECT_PATH} | sed 's#/#%2F#g')
URL_ESCAPED_IMAGECI_PATH=$(echo ${IMAGECI_PATH} | sed 's#/#%2F#g')
PROJECT_ID=$(curl -s https://gitlab.cern.ch/api/v4/projects/${URL_ESCAPED_PROJECT_PATH} | jq .id)
IMAGECI_ID=$(curl -s https://gitlab.cern.ch/api/v4/projects/${URL_ESCAPED_IMAGECI_PATH} | jq .id)
IMAGECI_PROJECT_ID=$(curl -s https://gitlab.cern.ch/api/v4/projects/${URL_ESCAPED_IMAGECI_PATH} | jq .id)
if [ -z $CI_PIPELINE_ID ]; then
  # Let's simply get the latest if we are not running through gitlabci
  PIPELINE_ID=$(curl -s https://gitlab.cern.ch/api/v4/projects/${PROJECT_ID}/repository/commits/${BRANCH} | jq .last_pipeline.id)
else
  PIPELINE_ID=$CI_PIPELINE_ID
fi
TRIGGERED_PIPELINE=$(curl -s https://gitlab.cern.ch/api/v4/projects/${PROJECT_ID}/pipelines/${PIPELINE_ID}/bridges | jq .'[0].downstream_pipeline.id')
JOB_URLS=$(curl -s https://gitlab.cern.ch/api/v4/projects/${IMAGECI_ID}/pipelines/${TRIGGERED_PIPELINE}/jobs |  jq '.[].web_url' | sed 's#"##g')
echo "Downloading artifacts from pipeline #${PIPELINE_ID} [${PROJECT_PATH}/${BRANCH}]"
echo "Artifact download directory is: ${TMPDIR}"
for j in $JOB_URLS; do
  JOB=$(echo "${j}" | rev | cut -d/ -f1 | rev)
  echo "Downloading artifacts for job: $JOB"
  curl -s -o $TMPDIR/${JOB}-artifacts.zip ${j}/artifacts/download
done
echo "Unzipping artifacts"
for file in ${TMPDIR}/*zip; do unzip -q -o $file -d ${TMPDIR}; done
echo "Log file <-> distribution"
grep run-cgk-tests ${TMPDIR}/*messages.log | grep -v puppet-agent | grep "Finished successfully on" | cut -d: -f1,6
echo "Displaying how many times a password reset was called (useful to compare across distributions)"
grep run-cgk-tests.sh ${TMPDIR}/*messages.log |grep -v puppet-agent | grep "password reset" | awk '{print $1}' | cut -d: -f1 |  uniq -c
echo "Displaying how many times 'Access Denied' was returned from msktuil (there should be zero)"
grep run-cgk-tests.sh ${TMPDIR}/*messages.log |grep -v puppet-agent | grep "Access denied" | awk '{print $1}' | cut -d: -f1 |  uniq -c
